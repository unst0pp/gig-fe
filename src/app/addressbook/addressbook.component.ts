import { Component, OnInit }                  from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiService }                         from '../services/api.service';
import { AppComponent }                       from '../app.component';

@Component({
    selector   : 'app-addressbook',
    templateUrl: './addressbook.component.html',
    styleUrls  : ['./addressbook.component.scss']
})

export class AddressbookComponent implements OnInit {

    showForm;
    title       = 'Addressbook';
    contactList = [];
    contactForm: FormGroup;
    requestResponse;
    editContactDetails;

    constructor(private apiService: ApiService, public appComponent: AppComponent, private formBuilder: FormBuilder) {

        this.contactForm = this.formBuilder.group({
            firstName: ['', [Validators.required]],
            lastName : ['', [Validators.required]],
            email    : ['', [Validators.required, Validators.email]],
            country  : ['', [Validators.required]]
        });
    }

    ngOnInit(): void {
        this.getAllContacts();
    }

    async getAllContacts() {
        await this.apiService.getAllContacts().subscribe(contacts => {
            this.contactList = contacts.result;
        });
    }

    showAddContactForm() {
        this.showForm        = 'new';
        this.title           = 'New Contact';
        this.requestResponse = null;

        this.contactForm.reset();
    }

    showEditContactForm(contactId) {
        this.showForm        = 'edit';
        this.title           = 'Edit Contact';
        this.requestResponse = null;

        this.editContactDetails = this.contactList.filter(contact => contact._id === contactId)[0];

        this.contactForm.patchValue({
            firstName: this.editContactDetails.firstName,
            lastName : this.editContactDetails.lastName,
            email    : this.editContactDetails.email,
            country  : this.editContactDetails.country
        });
    }

    createContact() {
        this.apiService.saveContact(this.contactForm.value).subscribe(saveResult => {
            this.closeForm();
            this.requestResponse = {
                message: saveResult.message,
                status : saveResult.status
            };
        }, error => {
            this.requestResponse = {
                message: error,
                status : 400
            };
        });
    }

    saveContact() {
        this.apiService.editContact(this.editContactDetails._id, this.contactForm.value).subscribe(editResult => {
            this.closeForm();
            this.requestResponse = {
                message: editResult.message,
                status : editResult.status
            };
        }, error => {
            this.requestResponse = {
                message: error,
                status : 400
            };
        });
    }

    deleteContact(contactId) {
        this.apiService.deleteContact(contactId).subscribe(deleteResult => {
            if (deleteResult.status === 200) {
                this.contactList = this.contactList.filter(contact => contact._id !== contactId);
            }

            this.requestResponse = {
                message: deleteResult.message,
                status : deleteResult.status
            };
        }, error => {
            this.requestResponse = {
                message: error,
                status : 400
            };
        });
    }

    closeForm() {
        this.title           = 'Addressbook';
        this.showForm        = null;
        this.requestResponse = null;

        this.getAllContacts();
    }

    get firstName() {
        return this.contactForm.get('firstName');
    }

    get lastName() {
        return this.contactForm.get('lastName');
    }

    get email() {
        return this.contactForm.get('email');
    }

    get country() {
        return this.contactForm.get('country');
    }
}
