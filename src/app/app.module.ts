import { CommonModule }                     from '@angular/common';
import { BrowserModule }                    from '@angular/platform-browser';
import { NgModule }                         from '@angular/core';
import { HttpClientModule }                 from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule }                        from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule }     from './app-routing.module';
import { AppComponent }         from './app.component';
import { AddressbookComponent } from './addressbook/addressbook.component';

@NgModule({
    declarations: [
        AppComponent,
        AddressbookComponent
    ],
    imports     : [
        CommonModule,
        BrowserModule,
        AppRoutingModule,
        NgbModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        FormsModule
    ],
    providers   : [],
    bootstrap   : [AppComponent]
})
export class AppModule {
}
