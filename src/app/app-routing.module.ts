import { NgModule }             from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddressbookComponent } from './addressbook/addressbook.component';

const routes: Routes = [{
    path     : '',
    component: AddressbookComponent,
    children : [
        {
            path     : '',
            component: AddressbookComponent
        }
    ]
}];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
