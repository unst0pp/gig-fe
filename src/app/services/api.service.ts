import { Injectable }                    from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError }        from 'rxjs';
import { catchError }                    from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class ApiService {
    apiUrl = 'http://localhost:1337/addressbook';

    constructor(private http: HttpClient) {
    }

    getAllContacts(): Observable<any> {
        return this.http.get(this.apiUrl)
                   .pipe(
                       catchError(this.handleError)
                   );
    }

    saveContact(contactDetails): Observable<any> {
        return this.http.post(this.apiUrl, contactDetails)
                   .pipe(
                       catchError(this.handleError)
                   );
    }

    editContact(id, contactDetails): Observable<any> {
        return this.http.put(this.apiUrl + '/' + id, contactDetails)
                   .pipe(
                       catchError(this.handleError)
                   );
    }

    deleteContact(id): Observable<any> {
        return this.http.delete(this.apiUrl + '/' + id)
                   .pipe(
                       catchError(this.handleError)
                   );
    }

    /** Error handling
     * =========================================================
     */
    handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            // A client-side or network error occurred
            console.error('An error occurred:', error.error.message);
        } else {
            // The backend returned an unsuccessful response code
            console.error(
                `API returned code ${ error.status }, ` +
                `body was:`);
            console.error(error);
        }
        return throwError('Something went wrong, please try again later.');
    }
}
